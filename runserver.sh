#!/bin/bash
echo "${0}: [1] Loading variables from .env file..."
source .env

echo "${0}: [2] Applying migrations..."
python manage.py makemigrations
python manage.py migrate

echo "${0}: creating admin user."
echo "from django.contrib.auth.models import User; print(\"Admin exists\") if User.objects.filter(username='admin').exists() else User.objects.create_superuser('admin', 'admin@admin.pl', 'admin')" | python manage.py shell

echo "${0}: [3] Collecting static files..."
python manage.py collectstatic --noinput

echo "${0}: [4] Running server..."
python manage.py runserver

