# Generated by Django 3.0 on 2019-12-16 16:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20191216_1515'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resource',
            name='date_of_purchase',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='price_per_item',
        ),
        migrations.RemoveField(
            model_name='resource',
            name='quantity',
        ),
        migrations.AddField(
            model_name='resource',
            name='units',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='Stuff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('quantity', models.IntegerField(default=1)),
                ('price_per_item', models.FloatField(verbose_name='Price per item (USD)')),
                ('date_of_purchase', models.DateTimeField()),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Resource')),
            ],
        ),
    ]
