from django.db import models

# Create your models here.
class Resource(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=50)
    symbol = models.CharField(max_length=10)
    units = models.CharField(max_length=10)

    Objects = models.Manager()

class Stuff(models.Model):
    def __str__(self):
        return self.name
        
    units = ['gram', 'ounce']
    name = models.CharField(max_length=50)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    price_per_item = models.FloatField("Price per item (USD)")
    date_of_purchase = models.DateTimeField()
