from django import forms
from django.forms import ModelForm
from .models import Stuff, Resource
from django.core import validators

AVAILABLE_UNITS = [
    ('gram','Ounces'),
    ('ounce','Grams')
]
BIRTH_YEAR_CHOICES = ['1980', '1981', '1982']

class DateInput(forms.DateInput):
    input_type = 'date'


class AddStuffForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        validators=[validators.MinLengthValidator(3)]
    )
    resource = forms.ModelChoiceField(
        queryset=Resource.Objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}),
        # validators=[validators.required]
    )
    units = forms.ChoiceField(
        choices=AVAILABLE_UNITS,
        widget=forms.Select(attrs={'class': 'form-control'})    
    )
    quantity = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        validators=[validators.MinValueValidator(1)]
    )
    date_of_purchase = forms.DateField(
        widget=DateInput(attrs={'class': 'form-control'}),
    )
    price_per_item = forms.FloatField(
        label='Price per item (USD):', 
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
        validators=[validators.MinValueValidator(1)]
    )
    