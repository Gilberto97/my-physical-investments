from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('stuff/', views.stuff_list, name='stuff_list'),
    path('add_stuff/', views.add_stuff_form, name='add_stuff_form'),
    path('stuff/<int:stuff_id>/', views.stuff_details, name='stuff_details'),
    path('resource/<str:resource_name>/', views.resource_details, name='resource_details'),
]
