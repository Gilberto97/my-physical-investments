# import pdb 
# pdb.set_trace()
# s, n, c
# api_key='bnpphuvrh5rccs6odcc0'
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime, date, timedelta
from django.views.generic import ListView
from .models import Resource, Stuff
from .forms import AddStuffForm
from .api import API
from plotly.offline import plot
from plotly.graph_objs import Scatter, Data, Figure, Layout


def index(request):
    
    x_data = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
    y_data = [x**2 for x in x_data]
    plot_div = plot([Scatter(x=x_data, y=y_data,
                            mode='lines', name='test',
                            opacity=0.8, marker_color='green')],
                            output_type='div')
    context={'plot_div': plot_div}
    return render(request, "index.html", context)


def stuff_list(request):
    stuff_list = Stuff.objects.all()

    today = datetime.today().replace(microsecond=0)
    
    stuff_objects = []
    for stuff in stuff_list:

        stuff_summary = API.get_stuff_summary(
            stuff.resource.symbol, 
            stuff.date_of_purchase, 
            today
        )

        value_change = round((stuff_summary['actual_price']-stuff_summary['previous_price'])*stuff.quantity,2)

        overpay = stuff.price_per_item - stuff_summary['previous_price'] 
        
        stuff_objects.append({
            'quantity': stuff.quantity,
            'name': stuff.name,
            'id': stuff.id,
            # 'resource': stuff.resource.name,
            'symbol': stuff.resource.symbol,
            'days_duration': stuff_summary.get('days_duration',0),
            'percent_change': stuff_summary.get('percent_change',0),
            'value_change': value_change, 
        })
    
    context = { 
        'stuff_objects': stuff_objects,
        'today': today.date(),
    }
    return render(request, 'stuff.html', context)


def stuff_details(request, stuff_id):
    stuff = Stuff.objects.get(id=stuff_id)

    today = datetime.today().replace(microsecond=0)

    stuff_summary = API.get_stuff_summary(
        stuff.resource.symbol, 
        stuff.date_of_purchase, 
        today
    )

    stuff_data = API.get_stuff_data(
        stuff.resource.symbol, 
        stuff.date_of_purchase, 
        today
    )

    value_change = round((stuff_summary['actual_price']-stuff_summary['previous_price'])*stuff.quantity,2)

    overpay = round(stuff.price_per_item - stuff_summary['previous_price'],2) 
    
    stuffObj = {
        'name': stuff.name,
        'resource': stuff.resource.name,
        'quantity': stuff.quantity,
        'price_per_item': stuff.price_per_item,
        'overpay': overpay,
        'date_of_purchase': stuff.date_of_purchase.date(),
        'symbol': stuff.resource.symbol,
        'units': stuff.resource.units,
        'value_change': value_change, 
        **stuff_summary,
    }

    dates = [datetime.fromtimestamp(timestamp).replace(hour=0) for timestamp in stuff_data['t']]
    prices = stuff_data['c']

    dates_num = len(dates)
    days_before = round(dates_num/3) if dates_num > 90 else 30

    before_data = API.get_stuff_data(
        stuff.resource.symbol, 
        stuff.date_of_purchase - timedelta(days=days_before), 
        stuff.date_of_purchase
    )

    before_dates = [datetime.fromtimestamp(timestamp).replace(hour=0) for timestamp in before_data['t']]
    before_prices = before_data['c']

    before_purchase = Scatter(
        x=before_dates, y=before_prices,
        mode='lines', name='Before Purchase',
        opacity=0.8, marker_color='grey',
    )
    
    real_purchase = Scatter(
        x=[dates[0]], y=[prices[0]],
        name='Real Purchase',
        opacity=0.8, marker_color='black',
    )
    
    my_purchase = Scatter(
        x=[dates[0]], y=[stuff.price_per_item],
        name='Your Purchase',
        opacity=0.8, marker_color='red',
    )

    after_purchase = Scatter(
        x=dates, y=prices,
        mode='lines', name='After Purchase',
        opacity=0.8, marker_color='black',
    )


    data = Data([before_purchase, real_purchase, my_purchase, after_purchase])

    layout=Layout( 
        title=f'{stuff.resource.symbol}/USD', 
        xaxis={'title':'Date'}, yaxis={'title':'Price (USD)'}
    )
    figure=Figure(data=data,layout=layout)
    div = plot(figure, auto_open=False, output_type='div')

    context={
        'stuff': stuffObj,
        'today': today.date(),
        'plot_div': div
    }
    return render(request, "stuff_details.html", context)


def add_stuff_form(request):

    form = AddStuffForm

    if request.method == 'POST':
        form = AddStuffForm(request.POST)

        if form.is_valid():
            print("validation success!")
            print(form.cleaned_data)
    context = { 
        'form': form,
    }
    return render(request, 'add_stuff_form.html', context)


def resource_details(request, resource_name):
    
    resource_name = resource_name.lower()

    resource = Resource.Objects.get(name=resource_name)

    today = datetime.today().replace(microsecond=0)

    resource_data = API.get_stuff_data(
        resource.symbol, 
        today - timedelta(days=4000), 
        today
    )

    resourceObj = {
        'name': resource.name,
        'symbol': resource.symbol,
        'units': resource.units,
    }

    dates = [datetime.fromtimestamp(timestamp).replace(hour=0) for timestamp in resource_data['t']]
    prices = resource_data['c']

    dates_num = len(dates)
    
    trace0 = Scatter(
        x=dates, y=prices,
        mode='lines', name=f'{resource.name}',
        opacity=0.8, marker_color='black',
    )


    data = Data([trace0])

    layout=Layout( 
        title=f'{resource.symbol}/USD', 
        xaxis={'title':'Date'}, yaxis={'title':'Price (USD)'}
    )
    figure=Figure(data=data,layout=layout)
    div = plot(figure, auto_open=False, output_type='div')

    context={
        'resource': resourceObj,
        'today': today.date(),
        'plot_div': div
    }
    return render(request, "resource_details.html", context)
