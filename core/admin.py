from django.contrib import admin
from .models import Resource, Stuff

admin.site.register(Resource)
admin.site.register(Stuff)
# Register your models here.
