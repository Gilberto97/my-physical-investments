import requests
from time import time, mktime, strptime
from datetime import datetime, date, timedelta
from pprint import pprint
import json

def get_day_name(date) -> str:
    return date.strftime("%A")

def set_friday_if_weekend(date):
    day_name = get_day_name(date)
    # calendar.
    if day_name == 'Saturday':
        return date - timedelta(days=1)
    elif day_name == 'Sunday':
        return date - timedelta(days=2)
    else: 
        return date

class API(object):

    api_token='bnpphuvrh5rccs6odcc0'

    @staticmethod
    def get_instruments() -> dict:
        url = f'https://finnhub.io/api/v1/forex/symbol?exchange=oanda&token={API.api_token}'
        response = requests.get(url)

        data = json.loads(response.text)
        instruments_dict = {}
        for item in data:
            symbol = item['displaySymbol'].split('/')
            if symbol[1] == 'USD':
                instruments_dict[symbol[0]] = item['description']
        
        return instruments_dict

    @staticmethod
    def get_stuff_data(symbol, start_date, end_date):

        diff_date = end_date.replace(tzinfo=None) - start_date.replace(tzinfo=None)
        start_date = set_friday_if_weekend(start_date)
        end_date = set_friday_if_weekend(end_date)

        start_timestamp = start_date.timestamp()
        end_timestamp = end_date.timestamp()

        url = f'https://finnhub.io/api/v1/forex/candle?symbol=OANDA:{symbol}_USD&resolution=D&from={start_timestamp}&to={end_timestamp}&token={API.api_token}'

        response = requests.get(url)
        data = json.loads(response.text)

        return data

    @staticmethod
    def get_stuff_summary(symbol, start_date, end_date):

        diff_date = end_date.replace(tzinfo=None) - start_date.replace(tzinfo=None)
        start_date = set_friday_if_weekend(start_date)
        end_date = set_friday_if_weekend(end_date)


        start_timestamp = start_date.timestamp()
        end_timestamp = end_date.timestamp()

        start_url = f'https://finnhub.io/api/v1/forex/candle?symbol=OANDA:{symbol}_USD&resolution=D&from={start_timestamp}&to={start_timestamp}&token={API.api_token}'
        end_url = f'https://finnhub.io/api/v1/forex/candle?symbol=OANDA:{symbol}_USD&resolution=D&from={end_timestamp}&to={end_timestamp}&token={API.api_token}'

        response = requests.get(start_url)
        start_data = json.loads(response.text)
        response = requests.get(end_url)
        end_data = json.loads(response.text)

        percent_change = ((end_data['c'][0]/start_data['c'][0]) * 100) - 100 

        return {
            'percent_change': round(percent_change,2),
            'actual_price': round(end_data['c'][0],2),
            'previous_price': round(start_data['c'][0],2),
            'days_duration': diff_date.days
        }

    @staticmethod
    def get_candles_objects(symbol='XAG', resolution='D', from_timestamp=str(time())[:10], to_timestamp=str(time())[:10]):

        url = f'https://finnhub.io/api/v1/forex/candle?symbol=OANDA:{symbol}_USD&resolution={resolution}&from={from_timestamp}&to={to_timestamp}&token={API.api_token}'
        response = requests.get(url)
        candles_data = json.loads(response.text)

        output = []
        candles_number = len(candles_data.get('c', []))
        
        status = candles_data['s']

        for i in range(candles_number):
            output.append({
                'open_price': candles_data['o'][i],
                'high_price':  candles_data['h'][i],
                'low_price':  candles_data['l'][i],
                'close_price':  candles_data['c'][i],
                'volume':  candles_data['v'][i],
                'timestamp':  candles_data['t'][i]
            })

        return output
